package com.babs.inscriptionservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaiementDTO {

    private Long paymentId;
    private String paymentStatus;
    private int montant;
    private String transactionId;
    private Long inscriptionId;
}
