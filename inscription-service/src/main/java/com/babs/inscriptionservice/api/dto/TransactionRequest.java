package com.babs.inscriptionservice.api.dto;


import com.babs.inscriptionservice.api.entity.Inscription;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    private Inscription inscription;
    private PaiementDTO paiementDTO;
}
