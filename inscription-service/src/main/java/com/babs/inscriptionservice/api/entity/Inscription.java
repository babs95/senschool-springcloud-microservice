package com.babs.inscriptionservice.api.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "inscription")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Inscription {
    @Id
    @GeneratedValue
    private Long id;
    private String dateInscription;
    @ManyToOne
    private Eleve eleve;

    private Long classeId;
}
