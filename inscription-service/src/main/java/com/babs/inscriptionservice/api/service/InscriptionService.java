package com.babs.inscriptionservice.api.service;

import com.babs.inscriptionservice.api.dto.PaiementDTO;
import com.babs.inscriptionservice.api.dto.TransactionRequest;
import com.babs.inscriptionservice.api.dto.TransactionResponse;
import com.babs.inscriptionservice.api.entity.Classe;
import com.babs.inscriptionservice.api.entity.Eleve;
import com.babs.inscriptionservice.api.entity.Inscription;
import com.babs.inscriptionservice.api.repository.InscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class InscriptionService {
    @Autowired
    private InscriptionRepository inscriptionRepository;
    @Autowired
    private EleveService eleveService;
    @Autowired
    private ClasseService classeService;
    @Autowired
    private RestTemplate template;

    public Inscription saveInsc(Inscription inscription){
        Eleve eleve = eleveService.saveEleve(inscription.getEleve());
        Optional<Classe> cl = classeService.getClasseById(inscription.getClasseId());
        inscription.setEleve(eleve);
        inscription.setClasseId(cl.get().getId());
        return inscriptionRepository.save(inscription);
    }

    public TransactionResponse saveInscription(TransactionRequest transactionRequest){
        String response ="";
        Inscription inscription = transactionRequest.getInscription();
        Eleve eleve = eleveService.saveEleve(inscription.getEleve());
        Optional<Classe> cl = classeService.getClasseById(inscription.getClasseId());
        inscription.setEleve(eleve);
        inscription.setClasseId(cl.get().getId());
        inscriptionRepository.save(inscription);

        PaiementDTO paiementDTO = transactionRequest.getPaiementDTO();
        paiementDTO.setInscriptionId(inscription.getId());
        paiementDTO.setMontant(1500);
        //rest call
       PaiementDTO paiementResponse = template.postForObject("http://PAIEMENT-SERVICE/payment/doPaiement", paiementDTO, PaiementDTO.class);
       response = paiementResponse.getPaymentStatus().equals("succes")?"Paiement réussi!":"Paiement échoué!";

       return new TransactionResponse(inscription, paiementResponse.getMontant(),paiementResponse.getTransactionId(), response);
    }
}
