package com.babs.paymentservice.api.repository;

import com.babs.paymentservice.api.entity.Paiement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaiementRepository extends JpaRepository<Paiement, Long> {
    Paiement findByInscriptionId(Long id);
}
