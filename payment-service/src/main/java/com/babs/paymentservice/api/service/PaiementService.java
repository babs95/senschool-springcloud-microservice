package com.babs.paymentservice.api.service;

import com.babs.paymentservice.api.entity.Paiement;
import com.babs.paymentservice.api.repository.PaiementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class PaiementService {

    @Autowired
    private PaiementRepository paiementRepository;

    public List<Paiement> getPaiements(){
        return  paiementRepository.findAll();
    }
    public Paiement doPaiement(Paiement paiement){
        paiement.setPaymentStatus(paymentProcessing());
        paiement.setTransactionId(UUID.randomUUID().toString());
        return paiementRepository.save(paiement);
    }

    public String paymentProcessing(){
        //simulation de la réponse d'un third party(papypal, orange money) lors d'un paiment
        return new Random().nextBoolean()?"success":"false";
    }

    public Paiement findPaiementHistoryByInscriptionId(Long id) {
        return paiementRepository.findByInscriptionId(id);
    }
}
