package com.babs.paymentservice.api.controller;

import com.babs.paymentservice.api.entity.Paiement;
import com.babs.paymentservice.api.service.PaiementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaiementService paiementService;

    @GetMapping(value ="/findPaiement")
    public List<Paiement> findAll(){
        return paiementService.getPaiements();
    }

    @PostMapping(value = "/doPaiement")
    public Paiement save(@RequestBody Paiement p){
        return paiementService.doPaiement(p);
    }

    @GetMapping(value ="/{id}")
    public Paiement findPaiementHistoryByInscriptionId(@PathVariable Long id){
        return paiementService.findPaiementHistoryByInscriptionId(id);
    }
}
